import { HttpUtility } from './utility'
import { HttpMetaApi } from './meta-api'
import { HttpMiddleware } from './middleware'

import { RobotApi } from './robot-api'

import { RobotUsecase } from './usecase-definitions'

class HttpApiOptions {
  appName: string = ''
  appDescription: string = ''
}

class HttpApi {
  options: HttpApiOptions
  util: HttpUtility

  mw: HttpMiddleware = new HttpMiddleware()
  meta: HttpMetaApi = new HttpMetaApi()

  robotApi: RobotApi = new RobotApi()

  constructor (options: HttpApiOptions) {
    this.options = options

    const util = new HttpUtility()
    util.appName = options.appName
    util.appDescription = options.appDescription

    this.util = util
    this.mw.util = util
    this.meta.util = util
    this.robotApi.util = util
  }

  setRobotUsecase (robotUsecase: RobotUsecase) {
    this.robotApi.robotUsecase = robotUsecase
  }
}

export {
  HttpApi,
  HttpApiOptions
}
