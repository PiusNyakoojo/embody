import * as express from 'express'
import * as fse from 'fs-extra'
import * as path from 'path'


class HttpUtility {
  appName: string = ''
  appDescription: string = ''

  render (req: express.Request, res: express.Response, status: number, data: any) {
    res.status(status)
    res.json(data)
  }

  renderImage (res: express.Response, image: string|Buffer, contentType?: string) {
    res.writeHead(200, {'Content-Type': contentType || 'image/png' })
    res.end(image, 'binary')
  }

  renderError (req: express.Request, res: express.Response, status: number, message: string) {
    res.status(status)
    res.json({
      message,
      success: false
    })
  }
}

export {
  HttpUtility
}
