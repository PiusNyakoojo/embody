import { DesktopRobotProvider } from './provider-definitions'

class RobotManagerOptions { }

class RobotManager {
  options: RobotManagerOptions
  desktopRobot?: DesktopRobotProvider

  constructor (options: RobotManagerOptions) {
    this.options = options
  }

  getScreenImage (): Promise<string|Buffer> {
    return this.desktopRobot!.getScreenImage()
  }

  startRecordingScreen (subscriberID: string, callback: (data: any) => void, options?: {}): void {
    this.desktopRobot!.startRecordingScreen(subscriberID, callback, options)
  }

  stopRecordingScreen (subscriberID: string): void {
    this.desktopRobot!.stopRecordingScreen(subscriberID)
  }

  moveMousePosition (dx: number, dy: number): void {
    this.desktopRobot!.moveMousePosition(dx, dy)
  }
}

export {
  RobotManager,
  RobotManagerOptions
}
