
import * as env from './env'

import httpServer from './http-server'
import webSocketServer from './websocket-server'

const envConfig = env.init()

const server = httpServer.init(envConfig)
const wsServer = webSocketServer.init(envConfig, server)

server.listen(envConfig[env.ENV_PORT])
