import { Robot } from '../../domain'

interface RobotUsecase {
  startRecordingScreen (subscriberID: string, callback: (data: any) => void, options?: {}): void
  stopRecordingScreen (subscriberID: string): void
}

export {
  RobotUsecase
}
