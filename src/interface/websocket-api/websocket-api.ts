
import { RobotApi } from './robot-api'

import { RobotUsecase } from './usecase-definitions'

class WebSocketApiOptions {}

class WebSocketApi {
  options: WebSocketApiOptions

  robotApi: RobotApi = new RobotApi()

  constructor (options: WebSocketApiOptions) {
    this.options = options
  }

  setRobotUsecase (robotUsecase: RobotUsecase) {
    this.robotApi.robotUsecase = robotUsecase
  }
}

export {
  WebSocketApi,
  WebSocketApiOptions
}
