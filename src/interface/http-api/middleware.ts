import * as path from 'path'
import * as fse from 'fs-extra'

import { Request, Response, NextFunction } from 'express'

import { HttpUtility } from './utility'

class HttpMiddleware {
  util: HttpUtility = new HttpUtility()

  ensureAuthorization (req: Request, res: Response, next: NextFunction) {
    const authHeader: string = req.headers['authorization'] || ''
    if (!authHeader) {
      this.util.renderError(req, res, 401, 'Requires Authentication: Invalid Bearer Header.')
      return
    }
    const authHeaderArr: string[] = authHeader.split(' ')
    const authSchema: string = authHeaderArr[0]
    const bearerToken: string = authHeaderArr[1]

    if (authSchema !== 'Bearer') {
      this.util.renderError(req, res, 401, 'Requires Authentication: Invalid Bearer Header.')
      return
    }

    if (!bearerToken) {
      this.util.renderError(req, res, 401, 'Requires Authentication: Invalid Bearer Header.')
      return
    }

    // Check bearer token for authenticity.
  }

  forceHTTPS (req: Request, res: Response, next: NextFunction) {
    if (req.headers['x-forwarded-proto'] === 'http') {
      res.writeHead(301, {
        Location: `https://${req.headers['host']}${req.url}`
      })
      res.end()
      return
    }

    next()
  }
}

export {
  HttpMiddleware
}
