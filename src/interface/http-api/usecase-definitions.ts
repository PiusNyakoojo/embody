import { Robot } from '../../domain'

// Define how a robot usecase behaves.
interface RobotUsecase {
  // getByUID (uid: string): Promise<Robot>
  // getByRID (aid: string): Promise<Robot>

  // create (robot: Robot): Promise<Robot>

  getScreenImage (): Promise<string|Buffer>
  moveMousePosition (dx: number, dy: number): void
}

export {
  RobotUsecase
}
