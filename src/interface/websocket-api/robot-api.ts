import { RobotUsecase } from './usecase-definitions'

class RobotApi {
  robotUsecase?: RobotUsecase

  startRecordingScreen (subscriberID: string, callback: (data: any) => void, options?: {}): void {

    this.robotUsecase!.startRecordingScreen(subscriberID, callback, options || {})
  }

  stopRecordingScreen (subscriberID: string): void {
    this.robotUsecase!.stopRecordingScreen(subscriberID)
  }
}

export {
  RobotApi
}
