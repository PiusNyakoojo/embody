
import { RobotDataManager } from '../domain'

class RobotUsecaseOptions { }

class RobotUsecase {
  options: RobotUsecaseOptions
  robotManager?: RobotDataManager

  constructor (options: RobotUsecaseOptions) {
    this.options = options
  }

  async getScreenImage (): Promise<string|Buffer> {
    return this.robotManager!.getScreenImage()
  }

  startRecordingScreen (subscriberID: string, callback: (data: any) => void, options?: {}): void {
    this.robotManager!.startRecordingScreen(subscriberID, callback, options)
  }

  stopRecordingScreen (subscriberID: string): void {
    this.robotManager!.stopRecordingScreen(subscriberID)
  }

  moveMousePosition (dx: number, dy: number): void {
    this.robotManager!.moveMousePosition(dx, dy)
  }
}

export {
  RobotUsecase,
  RobotUsecaseOptions
}
