# 🌎 embody

![npm version](https://badge.fury.io/js/embody.svg)
![downloads](https://img.shields.io/npm/dt/embody.svg)
![coverage](https://gitlab.com/piusnyakoojo/embody/badges/master/coverage.svg)
![pipeline](https://gitlab.com/piusnyakoojo/embody/badges/master/pipeline.svg)


`embody` is a sensor and actuator environment for training digital consciousness (also known as [artificial intelligence](https://en.wikipedia.org/wiki/Artificial_intelligence)) algorithms.

This project is a **work-in-progress** and does not completely work as suggested by this document. Parts of the system work as suggested, other parts are still being worked on.

## About
**Embody** is a set of interfaces to sensors and actuators on a computer. The interfaces provide a way to receive or update data about the state of a computer.

**Sensor interfaces** retrieve data about the state of a computer. For example: a screen capture video stream, mouse movement events and keyboard press events. **Actuator interfaces** update the state of a computer. For example: moving the mouse and pressing a keyboard button.

## Benefits
- 💪 **Flexible**: Learn from across any number of sense, action and interface configurations

## Sensor Features
- 👁️ **Sight**: See the world through a screen capture video stream, webcam or camera
- 👂 **Hearing**: Use a microphone to listen to the world (WIP)
- 🖐️ **Touch**: Feel through heat, texture and force feedback sensors (WIP)
- 👃 **Smell**: Respond to air quality sensors to model scent (WIP)
- 👅 **Taste**: Respond to water quality sensors to model taste (WIP)

## Actuator Features
- 🖼️ **Imagination**: Create images on a 2D canvas by painting with pixels (WIP)
- 🙌 **Movement**: Move in the world using a mouse, keyboard or motor
- 👄 **Speech**: Create voice patterns to be played by speakers (WIP)

## Limitations
- 🧪 **Experimental**: This project is a **work-in-progress**

## Screenshots

## Installation

#### Install the library
```
npm install embody -g
```

## Example Usecases

## API Reference

## Documentation
To check out project-related documentation, please visit [docs](https://gitlab.com/PiusNyakoojo/embody/blob/master/docs/README.md)

## Contributing
Feel free to join in. All are welcome. Please see [contributing guide](https://gitlab.com/PiusNyakoojo/embody/blob/master/CONTRIBUTING.md).

## Acknowledgements

## Learn More

## Related Work
Some well-known **artificial intelligence training environments** include: Conceptual Learning (VicariousAI [PixelWorld](https://github.com/vicariousinc/pixelworld)), Reinforcement Learning (OpenAI [Gym](https://github.com/openai/gym), DeepMind [Lab](https://github.com/deepmind/lab), [garage](https://github.com/rlworkgroup/garage)), Algorithm-Agnostic ([ROS](https://en.wikipedia.org/wiki/Robot_Operating_System), [Embody](https://gitlab.com/piusnyakoojo/embody))

## License
MIT
