import { Request, Response } from 'express'

import { HttpUtility } from './utility'

import { RobotUsecase } from './usecase-definitions'

interface GetScreenImageResponseV0 {
  image: any
  message: string
  success: boolean
}

interface MoveMousePositionResponseV0 {
  message: string
  success: boolean
}

class RobotApi {
  util: HttpUtility = new HttpUtility()
  robotUsecase?: RobotUsecase

  async getScreenImage (req: Request, res: Response) {
    const image = await this.robotUsecase!.getScreenImage()

    const response: GetScreenImageResponseV0 = {
      image,
      message: 'Successfully retrieved camera image!',
      success: true
    }

    this.util.renderImage(res, image)
  }

  moveMousePosition (req: Request, res: Response) {
    const dx: number = req.body.dx || 0
    const dy: number = req.body.dy || 0

    this.robotUsecase!.moveMousePosition(dx, dy)

    const response: MoveMousePositionResponseV0 = {
      message: 'Successfully moved mouse position!',
      success: true
    }

    this.util.render(req, res, 200, response)
  }
}

export {
  RobotApi
}
