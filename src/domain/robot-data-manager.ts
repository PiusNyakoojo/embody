
interface RobotDataManager {
  getScreenImage (): Promise<string|Buffer>
  startRecordingScreen (subscriberID: string, callback: (data: any) => void, options?: {}): void
  stopRecordingScreen (subscriberID: string): void
  moveMousePosition (dx: number, dy: number): void
}

export {
  RobotDataManager
}
