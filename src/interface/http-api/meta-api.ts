import { Request, Response } from 'express'

import { HttpUtility } from './utility'

interface GetIndexResponseV0 {
  message: string
  description: string
  versions: string
  success: boolean
}

const API_VERSIONS = ['v0']

class HttpMetaApi {
  util: HttpUtility = new HttpUtility()

  getIndex (req: Request, res: Response) {
    const response: GetIndexResponseV0 = {
      message: `Welcome to ${this.util.appName}`,
      description: this.util.appDescription,
      versions: `Supported api versions: ${API_VERSIONS.toString()}`,
      success: true
    }
    this.util.render(req, res, 200, response)
  }

  getCertBotKey (req: Request, res: Response) {
    res.write('place_certbot_key_here')
  }
}

export {
  HttpMetaApi
}
